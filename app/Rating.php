<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{

    protected $fillable = ['id', 'star', 'comment', 'article_id', 'user_id']; //Fillables fields from webform / rest client
    protected $hidden = ['created_at', 'updated_at'];
    //Creating the Rating relationshi,
    //a rating belongs to an article. Define an inverse one-to-many relationship with App\Post.
    public function article()
    {
      return $this->belongsTo('App\Article');
    }





}






?>
