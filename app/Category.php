<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = ['id','category_name', 'description']; //Fillables field from webform / rest client
    protected $hidden = ['created_at','updated_at']; 


    //creating the relationship between category and article.
    //Every article belongs to a category, and a category is associated with many Articles
    public function articles()
    {
        return $this->hasMany('App\Article');
    }

}






?>
