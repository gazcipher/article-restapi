<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    protected $fillable = ['id', 'user_id', 'title', 'message', 'category_id']; //Fillables fields from webform / rest client

    protected $hidden = ['created_at', 'updated_at'];


    //Creating the relationship between category and article
    //an article belongs to a category. Define a one-to-many relationship with App\Comment
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    //A single article can have multiple ratings
    //One to Many relationship
    public function ratings()
    {
        return $this->hasMany('App\Rating');
    }

}






?>
