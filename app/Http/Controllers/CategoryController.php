<?php

namespace App\Http\Controllers;
use App\Category;
use Validator;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.

     *
     * @return void
     */
    public function __construct()
    {

    }

    public function displayCategoryies()
    {
        return response()->json(Category::all(), 200); //Show available categories
    }

    public function displayCategory($id)
    {
        $category = Category::find($id);

        if(empty($category))
        {
            return response()->json(null, 400); //Bad request, Category could not be found
        }
        return response()->json($category, 200); //200 Ok success to a get a single Category
        //return response()->json(Category::find($id), 200);
    }

    public function store(Request $request)
    {
      //validate input to ensure input data are correct
      $this->validate($request, [
        'category_name' => 'required|string|max:64',
        'description' => 'string|max:255'
      ]);

        //create new Category resource
        //$createCategory = Category::create($request->all());

        $createCategory = new Category();

        $createCategory->category_name = $request->input('category_name');
        $createCategory->description = $request->input('description');

        $createCategory->save();

        return response()->json($createCategory, 201); //201 Create status for a successful resource creation
    }

    public function update(Request $request, $id)
    {
        //validate update request, check inputs
        $this->validate($request, [
          'category_name' => 'required|string|max:64',
          'description' => 'string|max:255'
        ]);

        //find the resource to update i.e the Category resource
        $categoryToUpdate = Category::findOrFail($id);

        if(empty($categoryToUpdate))
        {
            return response('Invalid category ID', 400); //Bad request
        }

        $categoryToUpdate->update($request->all());

        //return sucess response
        return response()->json($categoryToUpdate, 200); //200 Ok success


    }

    public function destroy($id)
    {
        $categoryToDelete = Category::find($id);

        //validate if not found throw bad request
        if(empty($categoryToDelete))
        {
            return response()->json(null, 400); //Bad Request, no Category with such id
        }

        //now delete if found
        $categoryToDelete->delete();

        return response('Deletion success', 204); //No content to return, item deleted
    }



}
