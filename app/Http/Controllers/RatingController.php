<?php

namespace App\Http\Controllers;
use App\Rating;
use App\Article;

use Validator;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index($article_id)
    {
        //$perPage = $request->input('per_page') ? : 10; //pagination
        //$ratings = Rating::orderBy('created_at', 'desc')->paginate($perPage);

        $article = Article::find($article_id); //get the article id

        if(empty($article))
        {
          return response('Invalid article id', 404); //Not found 404 request
        }

        //if found then we get associated ratings
        $ratings = $article->ratings;

        return response()->json($ratings, 200);
    }

    public function show($id)
    {
      // code...
    }

    public function rating(Request $request, $article_id)
    {
      $article = Article::find($article_id);
      if(!$article)
      {
        return response('Invalid Article Id', 404);
      }
        $this->validate($request, [
          'star' => 'required|integer',
          'comment' => 'string|max:255',
          'article_id' => 'integer',
          'user_id' => 'integer'
        ]);

        //create rating for this article 1 - 5 star
        $rating = Rating::create([
          'star' => $request->get('star'),
          'comment' => $request->get('comment'),
          'article_id' => $article_id,
          'user_id' => $this->getUserId()
        ]);

        return response()->json($rating, 201); //201 Created status for a success rating
    }

    public function update(Request $request, $article_id, $rating_id)
    {
        $rating = Rating::find($rating_id);//get the rating and article id to be updated
        $article = Article::find($article_id);

        if(!$rating || !$article)
        {
          return response('Rating or Article id is invalid', 400); //Bad request
        }

        //valid input
        $this->validate($request, [
          'star' => 'required|integer',
          'comment' => 'string|max:255',
          'article_id' => 'integer',
          'user_id' => 'integer'
        ]);

        $rating->star = $request->get('star');
        $rating->comment = $request->get('comment');
        $rating->user_id = $this->getUserId();
        $rating->article_id = $article_id;

        $rating->save(); //save changes to the db
        return response()->json($rating, 200); //Update success with 200 Status code.
    }

    public function destroy($id)
    {
      // code...
    }



}
