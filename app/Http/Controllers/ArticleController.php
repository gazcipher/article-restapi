<?php

namespace App\Http\Controllers;
use App\Article;
use Validator;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.

     *
     * @return void
     */
    public function __construct()
    {

    }

    public function displayArticles()
    {
        $articles = Article::orderBy('created_at', 'desc')->paginate(10);
        return response()->json($articles, 200); //Return json with 200 Ok of all articles
        //return response()->json(Article::all(), 200);
    }

    public function displayArticle($id)
    {
        $article = Article::find($id);
        if(empty($article))
        {
            return response()->json(null, 400); //Bad request, article could not be found
        }
        return response()->json($article, 200); //200 Ok success to a get a single article
        //return response()->json(Article::find($id), 200);
    }

    public function store(Request $request)
    {
      //validate input to ensure input data are correct
      $this->validate($request, [
        'message' => 'required|string|max:5000',
        'title' => 'required|string|max:160',
        'category_id' => 'integer',
        'user_id' => 'integer'
      ]);

        //create new article resource
        $createArticle = Article::create($request->all());

        return response()->json($createArticle, 201); //201 Create status for a successful resource creation
    }

    public function update(Request $request, $id)
    {
        //validate update request, check inputs
        $this->validate($request, [
          'message' => 'required|string|max:5000',
          'title' => 'required|string|max:160',
          'category_id' => 'integer',
          'user_id' => 'integer'
        ]);

        //find the resource to update i.e the article resource
        $articleToUpdate = Article::findOrFail($id);
        if(empty($articleToUpdate))
        {
            return response('Invalid Article ID', 400); //Bad request
        }

        //now update the article
        $articleToUpdate->update($request->all());

        //return sucess response
        return response()->json($articleToUpdate, 200); //200 Ok success


    }

    public function destroy($id)
    {
        $articleToDelete = Article::find($id);

        //validate if not found throw bad request
        if(empty($articleToDelete))
        {
            return response()->json(null, 400); //Bad Request, no article with such id
        }

        //now delete if found
        $articleToDelete->delete();

        return response('Deletion success', 204); //No content to return, item deleted
    }



}
