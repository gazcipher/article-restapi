<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\User;
use Gate;

class Controller extends BaseController
{
    //implementing the rule of DRY
    //Defining a re-usable functions in our status code and message
    public function success($data, $code)
    {
        return response()->json(['data' => $data], $code);
    }
    public function error($message, $code)
    {
        return response()->json(['message' => $message], $code);
    }

    /**
  * Get current authorized user id.
  * This method should be called only after validating the access token using OAuthMiddleware Middleware.
  *
  * @return boolean
  */
   protected function getUserId()
   {
     //return \LucaDegasperi\OAuth2Server\Facades\Authorizer::getResourceOwnerId();
   }
}
