<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create default categories data to be seeded into the db
      /*  $categories = array(
          ['category_name' => 'Football'],
          ['category_name' => 'Tennis'],
          ['category_name' => 'C#'],
          ['category_name' => 'PHP'],
          ['category_name' => 'JAVA'],
          ['category_name' => 'Python']
        );

        foreach ($categories as $category)
        {
            Category::create($category);
        }*/

        app('db')->table('categories')
          ->insert([
            'category_name' => 'Football',
            'description' => 'Football category',
          ]);

        app('db')->table('categories')
          ->insert([
            'category_name' => 'C#',
            'description' => 'C# Programming Language',
          ]);

        app('db')->table('categories')
          ->insert([
            'category_name' => 'PHP',
            'description' => 'PHP Programming Language',
          ]);
    }
}
