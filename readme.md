# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
# article-restapi
# article-restapi


#GET endpoints localhost:8000/api/v1/categories
#POST|GET endpoints localhost:8000/api/v1/articles
#GET|PUT endpoints localhost:8000/api/v1/articles/{id}
#POST http://localhost:8000/api/v1/articles/4/rating
#GET http://localhost:8000/api/v1/articles/4/ratings

#There is a know issue on github of migration for php 7.3** and Lumen,
#This is my firt lumen api, i will apprecate if i can get a feedback on the relationship between models, its return invalid formed relationship but this works fine in Laravel, what am i missing?
#I work well with Laravel, it seems there lots of moving part but it is something i have challenged myself to master in a month as this is very closed to Laravel.
