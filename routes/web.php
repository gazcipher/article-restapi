<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function() use ($router){
  //Category endpoint, just to list all available categories
    $router->get('categories', ['uses' => 'CategoryController@displayCategoryies']);
    $router->post('categories', ['uses' => 'CategoryController@store']);

    //Article endpoints
    $router->get('articles', ['uses' => 'ArticleController@displayArticles']);
    $router->get('articles/{id}', ['uses' => 'ArticleController@displayArticle']);
    //$router->get('articles/{id}', ['uses' => 'ArticleController@search']);
    $router->post('articles', ['uses' => 'ArticleController@store']);
    $router->put('articles/{id}', ['uses' => 'ArticleController@update']);
    $router->delete('articles/{id}', ['uses' => 'ArticleController@destroy']);

    //Rating endpoint
    $router->get('/articles/{article_id}/ratings', 'RatingController@index');
    $router->post('/articles/{article_id}/rating', 'RatingController@rating');
    $router->put('/articles/{article_id}/rating/{rating_id}', 'RatingController@update');
    //$router->post('articles/{id}/rating', ['uses' => 'ArticleController@rating']);

    // Access token route, Request an access token
    $router->post('/oauth/access_token', function() use ($router){
        return response()->json($router->make('oauth2-server.authorizer')->issueAccessToken());
    });

});
